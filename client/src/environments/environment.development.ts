export const environment = {
  clientId: '4574abec-ca70-4013-bdbd-0844de9783b1',
  apiAppId: '2b86d9fa-8b70-4459-a2c5-91ae7aa8a0ea',
  scope: 'api://2b86d9fa-8b70-4459-a2c5-91ae7aa8a0ea/access_as_user',
  redirectUri: 'http://localhost:4200',
  apiBaseUrl: 'https://localhost:7296/api',
};
