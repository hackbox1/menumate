import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { MenuItem } from './types';
import { environment } from 'src/environments/environment';

const API_ENDPOINT = environment.apiBaseUrl;

@Injectable({
    providedIn: 'root'
})
export class MealService {
    url = API_ENDPOINT;

    constructor(private http: HttpClient) { }

    getMenuItems() {
        console.log(this.url);
        return this.http.get<MenuItem[]>(this.url + '/meals');
    }

    getMenuItem(id: string) {
        return this.http.get<MenuItem>(this.url + '/' + id);
    }

    postMenuItem(MenuItem: MenuItem) {
        return this.http.post<MenuItem>(this.url, MenuItem);
    }

    deleteMenuItem(id: string) {
        return this.http.delete(this.url + '/' + id);
    }

    editMenuItem(MenuItem: MenuItem) {
        return this.http.put<MenuItem>(this.url + '/' + MenuItem.id, MenuItem);
    }
}