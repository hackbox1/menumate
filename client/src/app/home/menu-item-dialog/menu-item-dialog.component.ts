import { Component, Inject, SimpleChanges } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MenuItem } from 'src/app/types';

export type MenuItemDialogData = {
  menuItem: MenuItem;
};

@Component({
  selector: 'app-menu-item-dialog',
  templateUrl: './menu-item-dialog.component.html',
  styleUrls: ['./menu-item-dialog.component.scss'],
})
export class MenuItemDialogComponent {
  imageUrls: Array<string | ArrayBuffer> = [];

  constructor(
    public dialogRef: MatDialogRef<MenuItemDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: MenuItemDialogData
  ) {
    this.imageUrls = data.menuItem.imageUrls;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['data']) {
    }
  }

  onFileSelected(event: Event) {
    const element = event.currentTarget as HTMLInputElement;
    let file: File | undefined | null = element.files?.item(0);
    if (!file) return;

    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (event) => {
      this.imageUrls.push((<FileReader>event.target).result!);
    };
  }
}
