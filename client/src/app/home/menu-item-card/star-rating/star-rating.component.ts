import {
  Component,
  EventEmitter,
  HostListener,
  Input,
  Output
} from '@angular/core';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.scss'],
})
export class StarRatingComponent {
  @Input() rating: number = 0.0;
  @Output() updateRatingEvent = new EventEmitter<number>();

  hoverStars?: number;
  isHovering: boolean = false;

  getStarIcon(starId: number): string {
    const baseRating = this.isHovering ? this.hoverStars ?? 0 : this.rating;
    const starType = baseRating - starId;

    if (starType > 0.5) return 'star';
    if (starType === 0.5) return 'star_half';
    return 'star_outline';
  }

  getStarColor(starId: number): string {
    if (this.isHovering) {
      return starId >= (this.hoverStars ?? 0) ? 'lightgray' : 'gold';
    } else {
      return this.rating > 0 ? 'goldenrod' : 'darkgray';
    }
  }

  @HostListener('mousemove', ['$event'])
  onMousemove(event: MouseEvent) {
    const offsetInElement = event.offsetX;
    const offsetOutElement = (event.target as HTMLDivElement)?.offsetLeft;
    this.hoverStars = Math.min(
      5,
      Math.round(((offsetInElement + offsetOutElement) / 32) * 2) / 2
    );
  }

  @HostListener('mouseenter', ['$event'])
  onMouseEnter() {
    this.isHovering = true;
  }

  @HostListener('mouseleave', ['$event'])
  onMouseLeave() {
    this.isHovering = false;
  }

  @HostListener('click', ['$event'])
  onMouseClick() {
    if (this.isHovering && this.hoverStars) {
      this.updateRatingEvent.emit(this.hoverStars);
    }
  }
}
