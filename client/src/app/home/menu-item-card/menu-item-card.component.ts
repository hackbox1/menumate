import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MenuItem } from 'src/app/types';
import { MenuItemDialogComponent } from '../menu-item-dialog/menu-item-dialog.component';

@Component({
  selector: 'app-menu-item-card',
  templateUrl: './menu-item-card.component.html',
  styleUrls: ['./menu-item-card.component.scss'],
})
export class MenuItemCardComponent implements OnChanges {
  @Input() menuItem!: MenuItem;
  rating: number = 0.0;
  imageUrls: Array<string | ArrayBuffer> = [];

  constructor(private snackBar: MatSnackBar, public dialog: MatDialog) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['menuItem']) {
      this.rating = changes['menuItem'].currentValue.userRating;
      this.imageUrls = changes['menuItem'].currentValue.imageUrls;
    }
  }

  onClickDetails(): void {
    this.dialog.open(MenuItemDialogComponent, {
      data: { menuItem: this.menuItem },
      width: '600px',
    });
  }

  onUpdateRating(rating: number) {
    this.rating = rating;
    this.snackBar.open(
      `Your rating for ${this.menuItem.name} has been updated to ${rating}`,
      undefined,
      { duration: 2500 }
    );
  }

  onFileSelected(event: Event) {
    const element = event.currentTarget as HTMLInputElement;
    let file: File | undefined | null = element.files?.item(0);
    if (!file) return;

    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (event) => {
      this.imageUrls.push((<FileReader>event.target).result!);
    };
  }
}
