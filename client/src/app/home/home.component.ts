import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MenuItem } from '../types';
import { environment } from 'src/environments/environment';

const GRAPH_ENDPOINT = 'https://graph.microsoft.com/v1.0/me';

type ProfileType = {
  displayName?: string;
  givenName?: string;
  surname?: string;
  userPrincipalName?: string;
  id?: string;
};

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  profile!: ProfileType;
  todaysMenuItems: MenuItem[] = [];

  constructor(private http: HttpClient) {
    this.todaysMenuItems = [
      {
        id: 'c14aa1b3-d895-45f8-a110-b41b0d99c865',
        name: 'Cheeseburger',
        description:
          'A very tasty and delicious cheeseburger. A taste of freedom, you can just feel your arteries get stiffer as you eat.',
        imageUrls: [
          'https://images.pexels.com/photos/551991/pexels-photo-551991.jpeg',
          'https://images.pexels.com/photos/1639557/pexels-photo-1639557.jpeg',
        ],
        userRating: 3.5,
        averageRating: 2.4,
        ratingCount: 11,
        tags: ['meat', 'cheese', 'cholesterol'],
      },
      {
        id: 'e8d1fef7-c3c2-4ab4-9b71-2beeb0d82f02',
        name: 'Lemon cheesecake',
        description:
          'Cheesecake is a sweet dessert made with a soft fresh cheese, eggs, and sugar.',
        imageUrls: [
          'https://images.pexels.com/photos/1098592/pexels-photo-1098592.jpeg',
        ],
        userRating: 0.0,
        averageRating: 4.7,
        ratingCount: 33,
        tags: ['dessert', 'cake'],
      },
      {
        id: 'bda0068c-2b85-410d-94fb-b56b64742a54',
        name: 'Caesar salad',
        description:
          'A Caesar salad is a green salad of romaine lettuce and croutons dressed with lemon juice, olive oil, egg, Worcestershire sauce, anchovies, garlic, Dijon mustard, Parmesan cheese, and black pepper.',
        imageUrls: [
          'https://images.pexels.com/photos/406152/pexels-photo-406152.jpeg',
        ],
        userRating: 2.0,
        averageRating: 3.1,
        ratingCount: 7,
        tags: ['vegetarian', 'salad'],
      },
      {
        id: '6a7d7cd7-1df3-4133-b0d0-96bc40f57a2f',
        name: 'Balut',
        description:
          'Balut is a fertilized developing egg embryo that is boiled or steamed and eaten from the shell.',
        imageUrls: [],
        userRating: 0.0,
        averageRating: 0.0,
        ratingCount: 0,
        tags: ['eggs', 'disgusting'],
      },
    ];
  }

  getProfile() {
    this.http.get(GRAPH_ENDPOINT).subscribe((profile) => {
      this.profile = profile;
    });
  }

  testEcho() {
    this.http
      .get(`${environment.apiBaseUrl}/echo`, { responseType: 'text' })
      .subscribe((resp) => {
        console.log(resp);
      });
  }

  ngOnInit(): void {
    this.getProfile();
    this.testEcho();
  }
}
