export type MenuItem = {
  id: string;
  name: string;
  imageUrls: string[];
  description: string;
  tags: string[];
  userRating: number;
  averageRating: number;
  ratingCount: number;
};
