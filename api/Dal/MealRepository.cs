using api.Dal.Entities;
using api.Models;
using MongoDB.Driver;

namespace api.Dal;

public class MealRepository : IMealRepository
{
    // use mongodb driver to connect to mongodb
    private readonly IMongoCollection<Meal> _meals;

    public MealRepository(MenuMateDatabaseSettings settings)
    {
        var client = new MongoClient(settings.ConnectionString);
        var database = client.GetDatabase(settings.DatabaseName);

        _meals = database.GetCollection<Meal>("meals");
    }

    public async Task<IEnumerable<Meal>> GetMeals()
    {
        return await _meals.Find(meal => true).ToListAsync();
    }

    public async Task<Meal> GetMeal(string id)
    {
        return await _meals.Find<Meal>(meal => meal.Id == id).FirstOrDefaultAsync();
    }

    public async Task<Meal> AddMeal(Meal meal)
    {
        await _meals.InsertOneAsync(meal);
        return meal;
    }

    public async Task<Meal> UpdateMeal(string id, Meal meal)
    {
        await _meals.ReplaceOneAsync(m => m.Id == id, meal);
        return meal;
    }

    public async Task DeleteMeal(string id)
    {
        await _meals.DeleteOneAsync(m => m.Id == id);
    }

}