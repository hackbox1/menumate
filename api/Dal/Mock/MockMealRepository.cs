
using System.Text.Json;
using api.Dal.Entities;

namespace api.Dal.Mock;

public class MockMealRepository : IMealRepository
{
    public Task<Meal> AddMeal(Meal meal)
    {
        throw new NotImplementedException();
    }

    public Task DeleteMeal(string id)
    {
        throw new NotImplementedException();
    }

    public Task<Meal> GetMeal(string id)
    {
        throw new NotImplementedException();
    }

    public Task<IEnumerable<Meal>> GetMeals()
    {
        var mockHomePageView = McokHelper.ReadMockFile(MockConstants.MealsHomePageView);
        var meals = JsonHelper<IEnumerable<MealsHomePageDto>>.Deserialize(mockHomePageView);

        return Task.FromResult(meals.Select(m => new Meal
        {
            Id = m.Id,
            Name = m.Name,
            Description = m.Description,
            ImageUrl = m.ImageUrl
        }));
    }

    public Task<Meal> UpdateMeal(string id, Meal meal)
    {
        throw new NotImplementedException();
    }
}