namespace api.Dal.Mock;
public class MockConstants
{
    private static readonly string _mockPath = Path.Combine(Directory.GetCurrentDirectory(), "Models", "Mock");

    public static readonly string MealsHomePageView = Path.Combine(_mockPath, "MealsHomePageView.json");
}
public class McokHelper
{
    public static string ReadMockFile(string path) => File.ReadAllText(path);
}