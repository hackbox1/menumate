using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using api.Dal;
using api.Dal.Mock;
using api.Models;
using api.Services;
using api.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Identity.Web;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);
#region config
var settings = new AzureAdSetings();
builder.Configuration.Bind("AzureAd", settings);

var swaggerClientSettings = new SwaggerClientSettings();
builder.Configuration.Bind("SwaggerClient", swaggerClientSettings);

builder.Services.TryAddSingleton(settings);
builder.Services.TryAddSingleton(swaggerClientSettings);
#endregion

builder.Services.AddHttpContextAccessor();

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
   .AddMicrosoftIdentityWebApi(builder.Configuration.GetSection("AzureAd"));

builder.Services.Configure<MenuMateDatabaseSettings>(
    builder.Configuration.GetSection("MenuMateDatabase"));

builder.Services.AddScoped<IMealRepository, MockMealRepository>();
builder.Services.AddScoped<IMealService, MealService>();

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen(
    c =>
    {
        c.SwaggerDoc("v1", new OpenApiInfo { Title = "MenuMate Swagger Client", Version = "v1" });
        c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
        {
            Description = "Swagger for MenuMate",
            Name = "oauth2.0",
            Type = SecuritySchemeType.OAuth2,
            Flows = new OpenApiOAuthFlows
            {
                AuthorizationCode = new OpenApiOAuthFlow
                {
                    AuthorizationUrl = new Uri(swaggerClientSettings.AuthorizationUrl),
                    TokenUrl = new Uri(swaggerClientSettings.TokenUrl),
                    Scopes = new Dictionary<string, string>
                    {
                        {swaggerClientSettings.Scope, "Access API as User"}
                    }
                }
            }
        });
        c.AddSecurityRequirement(new OpenApiSecurityRequirement
        {
            {
                new OpenApiSecurityScheme
                {
                    Reference = new OpenApiReference{Type=ReferenceType.SecurityScheme, Id="oauth2"}
                },
                new [] {
                    swaggerClientSettings.Scope
                }
            }
        });
    });
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(s =>
   {
       s.OAuthClientId(swaggerClientSettings.ClientId);
       s.OAuthUsePkce();
       s.OAuthScopeSeparator(" ");
   });
}

app.UseHttpsRedirection();

app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
