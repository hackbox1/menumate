using api.Dal.Entities;

namespace api.Services.Interfaces;

public interface IMealService
{
    Task<IEnumerable<Meal>> GetMeals();
    Task<Meal> GetMeal(string id);
    Task<Meal> AddMeal(Meal meal);
    Task<Meal> UpdateMeal(string id, Meal meal);
    Task DeleteMeal(string id);
}
