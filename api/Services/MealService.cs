using api.Dal;
using api.Dal.Entities;
using api.Services.Interfaces;

namespace api.Services;

public class MealService : IMealService
{
    private readonly ILogger<MealService> _logger;
    private readonly IMealRepository _mealRepository;

    public MealService(ILogger<MealService> logger, IMealRepository mealRepository)
    {
        _logger = logger;
        _mealRepository = mealRepository;
    }

    public async Task<IEnumerable<Meal>> GetMeals()
    {
        return await _mealRepository.GetMeals();
    }

    public async Task<Meal> GetMeal(string id)
    {
        return await _mealRepository.GetMeal(id);
    }

    public async Task<Meal> AddMeal(Meal meal)
    {
        return await _mealRepository.AddMeal(meal);
    }

    public async Task<Meal> UpdateMeal(string id, Meal meal)
    {
        return await _mealRepository.UpdateMeal(id, meal);
    }

    public async Task DeleteMeal(string id)
    {
        await _mealRepository.DeleteMeal(id);
    }
}