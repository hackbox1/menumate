
public class MealsHomePageDto
{
    public string Id { get; set; } = string.Empty;
    public string Name { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public string ImageUrl { get; set; } = string.Empty;
    public double UserRating { get; set; }
    public double AverageRating { get; set; }
    public int RatingCount { get; set; }
    public List<string> Tags { get; set; } = Enumerable.Empty<string>().ToList();
}