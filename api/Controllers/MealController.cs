
using api.Dal.Entities;
using api.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers;

[ApiController]
[Route("api/[controller]")]
[Authorize]
public class MealsController : ControllerBase
{
    private readonly ILogger<MealsController> _logger;
    private readonly IMealService _mealService;

    public MealsController(ILogger<MealsController> logger, IMealService mealService)
    {
        _logger = logger;
        _mealService = mealService;
    }

    [HttpGet]
    public async Task<IActionResult> GetMeals()
    {
        var meals = await _mealService.GetMeals();
        return Ok(meals);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetMeal(string id)
    {
        var meal = await _mealService.GetMeal(id);
        return Ok(meal);
    }

    [HttpPost]
    [Authorize(Roles = "Admin")]
    public async Task<IActionResult> AddMeal([FromBody] Meal meal)
    {
        var newMeal = await _mealService.AddMeal(meal);
        return Ok(newMeal);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateMeal(string id, [FromBody] Meal meal)
    {
        var updatedMeal = await _mealService.UpdateMeal(id, meal);
        return Ok(updatedMeal);
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteMeal(string id)
    {
        await _mealService.DeleteMeal(id);
        return Ok();
    }
}