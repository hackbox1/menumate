using System.Text.Json;

public static class JsonHelper<T>
{
    public static T Deserialize(string json)
    {
        var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
        return JsonSerializer.Deserialize<T>(json, options) ??
            throw new ArgumentException("Unable to deserialize JSON.");
    }
}
